const EmailService = require('../services/EmailService');


/**
 * @api {get} /api/protected/emails?email=qwe&page=1 Find by email
 * @apiName Find by email
 * @apiGroup Emails
 *
 * @apiParam {String} email  слово для поиска, ищет такую подстроку, дефолтное значение - пустая строка (есть во всех адресах).
 * @apiParam {String} page номер стр, на стр 10 почт, дефолтное значение - 0.
 * 
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {json} data  написать.
 */
exports.findByEmail = async function (req, res) {
    try{
        const emails = await EmailService.findByEmail(req.queru);
        res.status(200).json({
            success: true,
            data: emails
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {get} /api/protected/emails/getAll Get all email
 * @apiName Get all email
 * @apiGroup Emails
 * 
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {json} data  написать.
 */
exports.getAll = async function (req, res) {
    try{
        const emails = await EmailService.getAll();
        res.status(200).json({
            success: true,
            data: emails
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {post} /api/protected/sendMail Send email
 * @apiName Send email
 * @apiGroup Emails
 *
 * @apiParam {Array} emails  массив email адресов для рассылки.
 * @apiParam {String} service сервис отправки: написать.....
 * @apiParam {String} email почта, с которой будет отправлено письмо.
 * @apiParam {String} password пароль от почты.
 * @apiParam {String} subject Заголовок сообщения.
 * @apiParam {String} text Текст сообщения.
 * 
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {json} data  написать.
 */
exports.sendEmail = async function (req, res) {
    try{
        const emails = await EmailService.sendEmail(req.body);
        res.status(200).json({
            success: true,
            data: emails
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {delete} /api/protected/emails/{:id} Delete email by id
 * @apiName Delete email by id
 * @apiGroup Emails
 *
 * @apiParam {String} id  id email для удаления.
 * 
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {json} data  написать.
 */
exports.deleteEmailByIdForAdmin = async function (req, res) {
    try{
        const emails = await EmailService.deleteEmailByIdForAdmin(req.params.id);
        res.status(200).json({
            success: true,
            data: emails
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

