const HoroscopeService = require('../services/HoroscopeService');

/**
 * @api {get} /api/horoscopes/?type=123&date={dateOfJs} Find by date and type
 * @apiName  Find by date and type
 * @apiGroup Horoscopes
 * 
 * 
 * @apiParam {String} type То, к какому знаку относиится.
 * @apiParam {Date} date Дата, на которую этот прогноз. 
 * 
 * 
 * @apiSuccess {json} data Созданый гороскоп.
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 */
exports.getByDateAndType = async function (req, res) {
    try{
        const horoscope = await HoroscopeService.getByDateAndType(req.query);
        res.status(200).json({
            success: true,
            data: horoscope
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}