const EmailService = require('../services/EmailService');

/**
 * @api {post} /api/emails/ Create
 * @apiName Create
 * @apiGroup Emails
 *
 * @apiParam {String} email  email, который введет пользователь.
 * @apiParam {String} type  типа его подписки, или например знак.
 * 
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {json} data  написать.
 */
exports.insert = async function (req, res) {
    try{
        const email = await EmailService.insert(req.body);
        res.status(200).json({
            success: true,
            data: email
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {put} /api/emails/{:id} Accept email
 * @apiName Accept email
 * @apiGroup Emails
 *
 * @apiParam {String} id все придет пользователю на почту, потом опишу.
 * 
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {json} data  написать.
 */
exports.acceptEmailById = async function (req, res) {
    try{
        const email = await EmailService.acceptEmailById(req.body);
        res.status(200).json({
            success: true,
            data: email
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {delete} /api/emails/{:id} Reject email
 * @apiName Reject email
 * @apiGroup Emails
 *
 * @apiParam {String} id все придет пользователю на почту, потом опишу.
 * 
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {json} data  написать.
 */
exports.deleteEmailById = async function (req, res) {
    try{
        const email = await EmailService.deleteEmailById(req.body);
        res.status(200).json({
            success: true,
            data: email
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}