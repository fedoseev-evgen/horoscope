const AdminService = require('../services/AdminService');

/**
 * @apiDefine Admin
 * 
 * 
 * @apiSuccess {String} data.name Имя админа.
 * @apiSuccess {String} data.login Логин админа.
 * @apiSuccess {String} data._id ID админа.
 */


/**
 * @api {post} /api/admin/ SignUp
 * @apiName SignUp  
 * @apiGroup Admin
 *
 * @apiParam {String} key  Ключ для создания админа
 * @apiParam {String} login  Логин (уникальный)
 * @apiParam {Array} password  Пароль, минимум 10 символов!
 * @apiParam {String} name  Имя пользователя, для его идентификации 
 * 
 * 
 * @apiParamExample {json/formData} Request-Example:
 * {
 *   "key": "Секрет",
 *   "login": "asd",
 *   "password": "1234567890",
 *   "name": "Евген"       
 * }
 * 
 *  
 * @apiSuccess {Bool} success  Успешность запроса, true - успешно.
 * @apiSuccess {jsonObject} data Админ.
 * @apiUse Admin
 * 
 * 
 * @apiSuccessExample {json} Success-Response:
 * {
 *   "success": true,
 *   "data": {
 *       "name": "Евген",
 *       "login": "asd",
 *       "_id": "5ce5afaf9ba7e53b046b531a"
 *   }
 *}
 */
exports.signUp = async function (req, res) {
    try{
        if(req.body.key === 'test'){
            const admin = await AdminService.signUp(req.body);
            res.status(200).json({
                success: true,
                data: admin
            });
        } else {
            res.status(415).json({
                success: false,
                message: 'Не верный ключ!'  
            })
        }
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {post} /api/admin/signIn SignIn
 * @apiName SignIn
 * @apiGroup Admin
 *
 * @apiParam {String} login  Логин (уникальный)
 * @apiParam {String} password  Пароль, минимум 10 символов!
 * 
 * @apiParamExample {json/formData} Request-Example:
 * {
 *   "login": "asd",
 *   "password": "1234567890" 
 * }
 * 
 * @apiSuccess {Bool} success  Успешность запроса, true - успешно.
 * @apiSuccess {String} data Токен для сессии.
 * 
 * @apiSuccessExample {json} Success-Response:
 * {
 *   "success": true,
 *   "data": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2U1YWZhZjliYTdlNTNiMDQ2YjUzMWEiLCJuYW1lIjoi0JXQstCz0LXQvSIsInRva2VuIjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SjBlWEJsSWpvaWNtVm1jbVZ6YUNJc0ltbGhkQ0k2TVRVMU9EVTFPRFl3TWl3aVpYaHdJam81TXpNME5UVTROakF5ZlEuOEp5TlZLNDZJc2lCVzhfVDB6aTV5U0RHQUxIQkIzNmw0SmJ1cUJZZjZDRSIsImlhdCI6MTU1ODU1ODYwMiwiZXhwIjoxNTYwMzU4NjAyfQ.6kZ0tAC4Nfhy2oGWQ6qAKsBqq3ok0J75uY6q2KL13CM"
 * }
 */
exports.signIn = async function (req, res) {
    try{
        const token = await AdminService.signIn(req.body);
        res.status(200).json({
            success: true,
            data: token
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {delete} /api/admin/{:id} Delete by id
 * @apiName Delete by id
 * @apiGroup Admin
 * 
 * @apiDescription В куках обязательно должен лежать токен и при запросе нужно давать права на просмотр куков!!
 * 
 * @apiParamExample {params} Request-Example:
 * {
 *   "id": "5cdc33f072665a6950a1d4ba"
 * }
 * 
 * 
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {jsonObject} data Удаленный админ.
 * @apiUse Admin
 *  
 * @apiSuccessExample {json} Success-Response:
 *{
 *  "success": true,
 *  "data": {
 *      "_id": "5cdc33f072665a6950a1d4ba",
 *      "name": "DROP TABLE tags",
 *      "login": "qwe1"
 *  }
 *}
 */
exports.deleteById = async function (req, res) {
    try{
        const admin = await AdminService.deleteById(req.params.id);
        if(admin === null) {
            res.status(404).json({
                success: false,
                message: 'Админ не найден!' 
            });
        } else {
            res.status(200).json({
                success: true,
                data: admin
            });
        }
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {get} /api/admin/getAll Get all
 * @apiName Get all
 * @apiGroup Admin
 * 
 * @apiDescription В куках обязательно должен лежать токен и при запросе нужно давать права на просмотр куков!!
 * 
 * @apiSuccessExample {json} Success-Response:
 * {
 *   "success": true,
 *   "data": [
 *       {
 *           "_id": "5cdfebde65d87729504e7a82",
 *           "name": "DROP TABLE tags",
 *           "login": "qwe1234"
 *       },
 *       {
 *           "_id": "5ce5afaf9ba7e53b046b531a",
 *           "name": "Евген",
 *           "login": "asd"
 *       }
 *   ]
 *}

 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 * @apiSuccess {jsonArray} data Массив всех админов.
 * @apiUse Admin
 */
exports.getAll = async function (req, res) {
    try{
        const admin = await AdminService.getAll();
        res.status(200).json({
            success: true,
            data: admin
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}