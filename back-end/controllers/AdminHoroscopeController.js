const HoroscopeService = require('../services/HoroscopeService');

/**
 * @api {post} /api/protected/horoscope/ Create
 * @apiName Create
 * @apiGroup Horoscopes
 * 
 * 
 * @apiParam {String} text Принимает строку, сохранит массив абзацев.
 * @apiParam {String} type То, к какому знаку относиится.
 * @apiParam {Date} date Дата, на которую этот прогноз. 
 * 
 * 
 * @apiSuccess {String} data Созданый гороскоп.
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 */
exports.insert = async function (req, res) {
    try{
        const horoscope = await HoroscopeService.insert(req.body);
        res.status(200).json({
            success: true,
            data: horoscope
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {put} /api/protected/horoscopes/{:id} Edit
 * @apiName Edit
 * @apiGroup Horoscopes
 * 
 * 
 * @apiParam {String} text Принимает строку, сохранит массив абзацев, не обязательно.
 * @apiParam {String} type То, к какому знаку относиится, не обязательно.
 * @apiParam {Date} date Дата, на которую этот прогноз, не обязательно. 
 * 
 * 
 * @apiSuccess {String} data Отредактированный гороскоп.
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 */
exports.edit = async function (req, res) {
    try{
        const horoscope = await HoroscopeService.edit(req.body);
        res.status(200).json({
            success: true,
            data: horoscope
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {delete} /api/protected/horoscopes/{id} Delete
 * @apiName Delete
 * @apiGroup Horoscopes
 * 
 * 
 * @apiParam {String} id ID конкретного гороскопа
 * 
 * 
 * @apiSuccess {String} data Удаленый гороскоп.
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 */
exports.delete = async function (req, res) {
    try{
        const horoscope = await HoroscopeService.delete(req.params.id);
        res.status(200).json({
            success: true,
            data: horoscope
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

/**
 * @api {get} /api/protected/horoscope/getByDate/{:date} Find by date
 * @apiName Find by date
 * @apiGroup Horoscopes
 * 
 * @apiParam {Date} date Дата, на которую этот прогноз. 
 * 
 * 
 * @apiSuccess {String} data Удаленый гороскоп.
 * @apiSuccess {Bool} success  успешность запроса, true - успешно.
 */
exports.getByDate = async function (req, res) {
    try{
        const horoscope = await HoroscopeService.getByDate(req.params.date);
        res.status(200).json({
            success: true,
            data: horoscope
        });
    } catch (err) {
        res.status(err.status || 500).json({
            success: false,
            message: err.message
        });
    }
}

