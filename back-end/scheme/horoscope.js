const mongoose = require('mongoose');
const horospeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    text:{
        type:Array,
        required:true
    },
    type:{
        type: String,
        required:true,
    },
    date:{
        type:Date,
        required:true,
    },
},{
    collection: "horospeSchema",
    versionKey: false
});



horospeSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('horospeSchema', horospeSchema);