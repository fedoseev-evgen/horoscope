const mongoose = require('mongoose');
const emailSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    email:{
        type:String,
        required:true
    },
    success:{
        type: Boolean,
        default: false,
    },
    type:{
        type: String,
        required: true
    },
    date:{
        type: Date,
        default: new Date()
    }
},{
    collection: "emailSchema",
    versionKey: false
});

emailSchema.pre('save', function(next) {
    if(this._id===null||this._id===undefined)
    this._id = new mongoose.Types.ObjectId();
    next();
});

module.exports = mongoose.model('emailSchema', emailSchema);