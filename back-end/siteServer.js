const express = require('express');
const http = require('http');
const config = require('./libs/config');

// const options = {
//     key: fs.readFileSync('/etc/letsencrypt/live/it-student.ru/privkey.pem'),
//     cert: fs.readFileSync('/etc/letsencrypt/live/it-student.ru/cert.pem')
//   };

const app = express();
app.use('/', express.static('./front-end/build'));
app.use(function (req, res) {
    res.status(404).send("Not Found");
});

const httpServer = http.createServer(app);

function onListening() {
    console.log('Listening on port ', config.port);
}

httpServer.on('listening', onListening);
httpServer.listen(config.port, config.ipForSite);
