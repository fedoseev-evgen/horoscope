const Horoscope = require('../scheme/horoscope');
const config = require('../libs/config');


/**
 * @param {Object} arguments
 * @param {String} arguments.text - Принимает строку, сохранит массив абзацев
 * @param {String} arguments.type - То, к какому знаку относиится 
 * @param {Date} arguments.date - Имя пользователя, для его идентификации 
 */
exports.insert = async function ( {text, type, date} ) {
    if(!text) throw {status: 415, message: 'Нет текста!'}
    if(!date) throw {status: 415, message: 'Нет даты!'}
    if(!type) throw {status: 415, message: 'Нет типа!'}
    const horoscope = new Horoscope({
        text: text.split('\n'),
        date: new Date(date.getFullYear(),date.getMonth(),date.getDate()),
        type: type
    });
    try{
        const resHoroscope = saveHoroscopePromise(horoscope);
    } catch (err) {
        throw err;
    }
    return resHoroscope;
}

/**
 * @param {Object} arguments
 * @param {String} arguments.type - То, к какому знаку относиится 
 * @param {Date} arguments.date - Имя пользователя, для его идентификации 
 */
exports.getByDateAndType = async function ({date, type}) {
    let newDate = new Date(date.getFullYear(),date.getMonth(),date.getDate());
    let horoscope = await Horoscope.findOne({type:type,date:newDate}).exec();
    if (horoscope) {
        return horoscope
    }
    throw {
        status: 404,
        message: "Гороскоп не найден"
    }
}

/**
 * @param {Object} arguments
 * @param {Date} arguments.date - Имя пользователя, для его идентификации 
 */
exports.getByDateAndType = async function ({date}) {
    let newDate = new Date(date.getFullYear(),date.getMonth(),date.getDate());
    let horoscope = await Horoscope.find({date:newDate}).exec();
    if (horoscope) {
        return horoscope
    }
    throw {
        status: 404,
        message: "Гороскопы не найдены"
    }
}

/**
 * @param {Object} arguments
 * @param {Date} arguments.date - Имя пользователя, для его идентификации 
 * @param {String} arguments.type - Имя пользователя, для его идентификации 
 * @param {String} arguments.text - Имя пользователя, для его идентификации 
 * @param {String} arguments.id - Имя пользователя, для его идентификации 
 */
exports.edit = async function ({date, type, text, id}) {
    if ( !text ) text = text.split('\n');
    if( !date ) date = new Date(date.getFullYear(),date.getMonth(),date.getDate());
    let horoscope = await Horoscope.findOne({date:newDate}).exec()
    .then(res => {
        if (!res) return null;
        Horoscope.updateOne({_id:id}, {$set : {type: type || horoscope.type, text: text || horoscope.text, date: date || horoscope.date }});
    })
    .catch(err => null);
    if (horoscope) {
        return horoscope
    }
    throw {
        status: 404,
        message: "Гороскопы не найдены"
    }
}

/**
 * @param {String} id
 */
exports.delete = async function ( id ) {
    let horoscope = await Horoscope.deleteOne({_id:id}).exec();
    if (horoscope) {
        return horoscope
    }
    throw {
        status: 404,
        message: "Гороскоп не найден"
    }
}


// exports.findByPage = function ( { page, type } ) {
//     Horoscope.find({type: type}).sort({date:1}).skip((page-1)*10).limit(10).exec()
// }

function saveHoroscopePromise(horoscope) {
    return new Promise((resolve, reject)=> {
        horoscope.save(function (err, result){
            if(err){
                reject({
                    status:500,
                    message: "Гороском не создан!"
                });
            } else {
                resolve(result);
            }
        });
    })
}