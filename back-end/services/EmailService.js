const Email = require('../scheme/email');
const sendEmail = require('../libs/sendMail');


/**
 * @param {Object} arguments -  
 * @param {String} arguments.email - email пользователя
 * @param {String} arguments.type - тип пользователя 
 */
exports.insert = async function ( { email, type } ) {
    if( !email ) throw {
        status: 415,
        message: 'Нет почты!'
    }
    const newType = type || 'не указано';
    if( email.indexOf('@') === -1 || email.lastIndexOf('@') !== email.indexOf('@') || email.indexOf('.') === -1) throw {
        status: 415, 
        message: 'Почта не валидна!'
    }
    const newEmail = new Email({
        email:email,
        type: newType
    });

    const newEmailAfterSave = emaiSavePromise(newEmail)
    .then(result => result)
    .catch(err => err);

    if(newEmailAfterSave.message) {
        throw newEmailAfterSave
    } 
    return newEmailAfterSave;
}

/**
 * @param {String} email - email пользователя (можно не полностью)
 */
exports.findByEmail = async function ( email ) {
    if( !email ) throw {
        status: 415,
        message: 'Нет почты!'
    }
    const emails = await Email.find({success:true, email: {$regex:email, $options:'i'}}).limit(10).skip((page?page:0)*10).exec()
    .then(result => result === null? []:result)
    .catch(err => {
        console.log(err);
        return []
    });
    return emails;
}

exports.getAll = async function () {
    const emails = await Email.find().exec()
    .then(result => result === null? []:result)
    .catch(err => {
        console.log(err);
        return []
    });
    return emails;
}

/**
 * @param {String} email - email пользователя (можно не полностью)
 */
exports.acceptEmailById = async function ( {id} ) {
    if( !id ) throw {
        status: 415,
        message: 'Нет id!'
    }
    const email = await Email.findByIdAndUpdate(id, {$set:{success: true}} ).exec()
    .then(result => result)
    .catch(err => {
        return{
            status:500,
            message: 'Сообщение не отправилось!'
        }
    });
    if(email.status)
    throw email;
    return email;
}

/**
 * @param {String} email - email пользователя (можно не полностью)
 */
exports.deleteEmailById = async function ( {id} ) {
    if( !id ) throw {
        status: 415,
        message: 'Нет id!'
    }
    const email = await Email.findOneAndRemove( { _id: id, success: false } ).exec()
    .then(result => result)
    .catch(err => {
        return{
            status:500,
            message: 'Сообщение не отправилось!'
        }
    });
    if(email.status)
    throw email;
    return email;
}

/**
 * @param {String} email - email пользователя (можно не полностью)
 */
exports.deleteEmailByIdForAdmin = async function ( {id} ) {
    if( !id ) throw {
        status: 415,
        message: 'Нет id!'
    }
    const email = await Email.findOneAndRemove( { _id: id } ).exec()
    .then(result => result)
    .catch(err => {
        return{
            status:500,
            message: 'Сообщение не отправилось!'
        }
    });
    if(email.status)
    throw email;
    return email;
}

/**
 * @param {String} email - email пользователя (можно не полностью)
 */
exports.sendEmail = async function ( {emails =[], email='', password='',service='',text,subject} ) {
    if( !emails || emails === [] ) throw {
        status: 415,
        message: 'Нет почты!'
    }
    if( !text ) throw {
        status: 415,
        message: 'Нет текста сообщения!'
    }
    if( !subject ) throw {
        status: 415,
        message: 'Нет заголовка сообщения!'
    }
    let messages = [];
    for (let index in emails) {
        messges.push(sendEmail({
            service:service,
            from: email,
            passsword: password,
            to:emails[index],
            text:text,
            subject:subject
        }));
    }
    return messages;
}

function emaiSavePromise(email) {
    return new Promise((resolve, reject)=> {
        email.save(function (err, result){
            if(err){
                reject({
                    status:500,
                    message: "Не удалось сохранить почту!"
                });
            } else {
                resolve(result);
            }
        });
    })
}