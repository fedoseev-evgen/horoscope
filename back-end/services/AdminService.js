const Admin = require('../scheme/admin');
const config = require('../libs/config');
const jwt = require('jsonwebtoken');

/**
 * @param {Object} arguments
 * @param {Array} arguments.password - Пароль, минимум 10 символов!
 * @param {String} arguments.login - Логин (уникальный)
 * @param {String} arguments.name - Имя пользователя, для его идентификации 
 */
exports.signUp = async function ( {login, password, name} ) {
    if(!login) throw { status:415, message:"Нет логина!" };
    if(!password) throw { status:415, message:"Нет пароля!" };
    if(password.length < 10) throw { status:415, message:"Длина пароля менее 10 символов!" };
    if(!name) throw { status:415, message:"Нет имени!" };
    //Написать ошибку сохранения
    if(!await Admin.checkLogin(login)) throw { status: 415, message: "Данный логин уже занят!" };

    var newAdmin = new Admin({
        name: name,
        password: password,
        login: login
    });

    let adminAfterSave =await saveAdminPromise(newAdmin)
    .then(res => res)
    .catch(err=> err);

    adminAfterSave.token = undefined;
    adminAfterSave.salt = undefined;
    adminAfterSave.hashPassword = undefined;
    if(adminAfterSave.err) {
        throw adminAfterSave.err;
    } else {
        return adminAfterSave;
    }
}

exports.signIn = async function ({ login, password }) {
    try{
        if(!login) throw {status:415, message: 'Нет логина!'};
        if(!password) throw{status:415, message: 'Нет пароля!'};
        const admin = await Admin.findOne({login:login}).exec()
        .then(payload => {
            if (payload === null) {
                throw {status:404, message:'Такой логин не существует!'};
            } else
            if (payload.encryptPassword(password) !== payload.hashPassword) {
                throw {status: 404, message:'Неверный пароль!'};
                //Поправить
            } else
                return payload;
        })
        .then(payload => {
            const refreshToken = jwt.sign({type: 'refresh'}, config.jwt.secretOrKey, { expiresIn: config.jwt.expiresInRefreshToken });
                Admin.update({ login: payload.login }, { $set: {token: refreshToken} }, (err) => {
                    if (err) log.error(err);
                });
                const tokenInfo = {
                    _id: payload._id,
                    name: payload.name,
                    token: refreshToken
                };
                const token = jwt.sign(tokenInfo, config.jwt.secretOrKey, {expiresIn: config.jwt.expiresIn});
                return token
        })
        .catch(err => {
            throw err;
        })
        return admin;
    } catch (err){
        throw err;
    }
}

/**
 * @param {String} id - ID того, кого удалять 
 */
exports.deleteById = async function ( id ) {
    if(!id) throw { status:415, message:"Нет ID!" };

    try{
        let adminAfterRemove =await findByIdAndRemovePromise(id)
        .then(res => res)
        .catch(err=> err);
        if(adminAfterRemove){
            adminAfterRemove.token = undefined;
            adminAfterRemove.salt = undefined;
            adminAfterRemove.hashPassword = undefined;
        }
        return adminAfterRemove;
    } catch(err) {
        throw err;
    }
}

exports.getAll = async function () {
    return await Admin.find({}, { token: 0, salt:0, hashPassword:0 }).exec()
    .then(res => res)
}

//Промисифицированные функции

function saveAdminPromise(admin) {
    return new Promise((resolve, reject)=> {
        admin.save(function (err, result){
            if(err){
                reject({
                    status:500,
                    message: "Админ не создан!"
                });
            } else {
                resolve(result);
            }
        });
    })
}

function findByIdAndRemovePromise(id) {
    return new Promise((resolve, reject)=> {
        Admin.findOneAndRemove({_id: id},function (err, result){
            if(err){
                reject({
                    status:500,
                    message: "Админ не удален!"
                });
            } else {
                resolve(result);
            }
        });
    })
}