var nodemailer = require('nodemailer'); 
module.exports = async function ( {service, from, passsword, subject, text, to} ) {
    var transporter = nodemailer.createTransport({
        service: service,
        auth: {
            user: from,
            pass: passsword
        }
    });
    var mailOptions = {
        from: from,
        to: to,
        subject: subject,
        text: text
    }
    let newMail = await sendMail(transporter, mailOptions)
    .then(res => res);
    if(newMail.err)
    return newMail;
}

function sendMail(transporter, mailOptions) {
    return new Promise((resolve, reject)=> {
        transporter.sendMail(mailOptions, function(err, info){
            if(err){
                resolve('Сообщение не отправлено на почту ' + mailOptions.to);
            } else {
                resolve('Сообщение успешно отправлено: ' + info.response);
            }
        });
    })
}