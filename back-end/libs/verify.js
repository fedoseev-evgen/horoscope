const jwt = require('jsonwebtoken');
const config = require('./config');
const Admin = require('./../scheme/admin');
module.exports = function (req, res, next){
    try{
        const token = req.cookies.token || req.body.token; 
        jwt.verify(token, config.jwt.secretOrKey, function (err, decoded) {
            if (decoded !== undefined){
                Admin.findOne({token:decoded.token},function (error, admin){
                    if(error || admin==null){
                        res.status(401).json({
                            success: false,
                            message: 'Вы не авторизированны'
                        });
                    }else{  
                        req.admin = admin;
                        next()
                    }
                })
            } else  if (err) {
                res.status(401).json({
                    success: false,
                    message: 'Вы не авторизированны'
                });
            }
        });
    } catch (err) {
        if (err.message === "Cannot read property 'refreshToken' of null"){
            res.status(401).json({
                success: false,
                message: 'Вы не авторизированны'
            });
        } else {
            res.send(err.message);
        }
    }
}
