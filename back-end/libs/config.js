﻿var config = {};
const ExtractJwt = require("passport-jwt").ExtractJwt;

config.port = 80;

config.outIP="http://127.0.0.1";
config.ip = "127.0.0.1";
config.ipForSite = "http://mir-goroskopov.ru";

config.jwt= {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('bearer'),

    secretOrKey: '7DDJSu8UAvwfzRwjNgB5K4q4',

    expiresIn: 60 * 30 * 1000,

    expiresInRefreshToken: 1000 * 60*60*24*90,
};


config.mongoose = {
    url: "mongodb://127.0.0.1/horoscope",
};


module.exports=config;