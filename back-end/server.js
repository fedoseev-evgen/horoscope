﻿const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
// const https = require('https');
const compression = require('compression');
// const multer = require('multer');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
// const path = require('path');
const config = require('./libs/config');
const indexRoutes = require('./routes/index');


mongoose.connect(config.mongoose.url, { useNewUrlParser: true , useCreateIndex: true }, function (err) {
if (err)
    return console.log(err);
    mongoose.set('useFindAndModify', false);
    // const options = {
    //     key: fs.readFileSync('/etc/letsencrypt/live/it-student.ru/privkey.pem'),
    //     cert: fs.readFileSync('/etc/letsencrypt/live/it-student.ru/cert.pem')
    //   };

const app = express();
app.use(cookieParser());
app.use(compression());
app.set('view engine', 'ejs');
app.use('/files', express.static('./files'));
// app.use('/api', express.static('./public/api'));
app.use('/api', express.static('./back-end/public/api'));

app.use(bodyParser.urlencoded({
    extended: false,
    limit: '5mb'
}));

// app.use(multer(
//     {
//         dest: path.join(__dirname, 'public/uploads'),
//         limits: {
//             files:5,
//         }
//     }
// ).any());

//На время, пока не будет прокси
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Headers", "Content-Type");
    next();
});

app.use('/', (req, res, next) => {
    console.log("Body: \n",req.body);next();
    console.log(req.url);
}, indexRoutes);

app.use(function(req, res){
    res.status(404).send("Not Found");
});

const httpServer = http.createServer(app);
function onListening(){
    console.log('Listening on port ', config.port);
}
    httpServer.on('listening', onListening);
    httpServer.listen(config.port,config.ip);
});
