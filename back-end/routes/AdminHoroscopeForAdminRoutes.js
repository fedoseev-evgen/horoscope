const express = require('express');
const router = express.Router();
const AdminHoroscopeController = require('../controllers/AdminHoroscopeController');
const verify = require('../libs/verify');

router.post('/', verify, AdminHoroscopeController.insert);

module.exports = router;