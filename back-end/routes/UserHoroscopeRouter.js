const express = require('express');
const router = express.Router();
const UserHoroscopeController = require('../controllers/UserHoroscopeController');

router.get('/', UserHoroscopeController.getByDateAndType);
// router.post('/signIn', AdminController.signIn);

module.exports = router;