﻿const express = require('express');
const router = express.Router();
const AdminRoutes = require('./AdminRouter');
const AdminHoroscopeForAdminRoutes = require('./AdminHoroscopeForAdminRoutes');
const UserHoroscopeRouter = require('./UserHoroscopeRouter');


router.get('/', (req, res)=> {
    res.send('Hello');
});
router.post('/', (req, res)=> {
    res.send('Hello');
});
router.use('/api/admin', AdminRoutes);
router.use('/api/horoscopeForAdmin', AdminHoroscopeForAdminRoutes);
router.use('/api/horoscopeForUser', UserHoroscopeRouter);

module.exports = router;
