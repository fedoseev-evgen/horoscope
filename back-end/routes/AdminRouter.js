const express = require('express');
const router = express.Router();
const AdminController = require('../controllers/AdminController');
const verify = require('../libs/verify')

router.post('/', AdminController.signUp);
router.post('/signIn', AdminController.signIn);
router.get('/getAll', verify, AdminController.getAll);
router.delete('/:id', verify, AdminController.deleteById);

module.exports = router;