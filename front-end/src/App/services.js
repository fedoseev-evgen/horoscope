let mounts = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

export function getToday() {
    let today = new Date();
    return today.getDate() + ' ' + mounts[today.getMonth()] + ' ' + today.getFullYear() + ' ' + 'года';
}

//в 2033 году надо будет исправить функцию
export function moonPhase() {
    let today = new Date();
    let L = 1+(today.getFullYear()-2014);
    let D = today.getDate();
    let M = today.getMonth()+1;
    let N = (L * 11) - 14 + D + M;
    while (N>30){
        N-=30;
    }
    if (N===1){return 'Новолуние, ' + N + ' лунные сутки'}
    else if (N>1 && N<16){return 'Растущая луна, ' + N + ' лунные сутки'}
    else if (N===16) {return 'Полнолуние, ' + N + ' лунные сутки'}
    else if (N>16){return 'Убывающая луна, ' + N + ' лунные сутки'}
    else {}
};



