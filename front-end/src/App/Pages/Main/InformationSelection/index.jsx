import React, {Component} from 'react';
import './style.scss'
import classNames from 'classnames/bind';
import {zodiacSign} from "../../../DB";

class InformationSelection extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sign: false,
            date: false
        };
    }

    render() {

        let table = [
            ['На 2020 год', 'На месяц', 'На сегодня', 'На год'],
        ];


        return <div className='information-selection-content'>
            <div className='container'>
                <form className='row' name='signs'>
                    {
                        zodiacSign.map((sign, index) => {
                            return <div className='item' key={index}>
                                <input className='point' type='radio' name='sign'/>
                                {sign.name}
                            </div>
                        })
                    }
                </form>

            </div>
        </div>
    }
}

export default InformationSelection;
