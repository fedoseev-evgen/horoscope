import React, {Component} from 'react';
import moon from './../../../Static/Media/moon.png'
import {moonPhase} from "../../../services";
import './style.scss'

class Moon extends Component{
    render() {
        return<div className='moon-content'>
            <div className='container'>
                <div className='text'>
                    <h2>{moonPhase()}</h2>
                    <h3>Лунный календарь</h3>
                </div>
                <img className='moon-img' src={moon}/>
            </div>
        </div>
    }
}

export default Moon;
