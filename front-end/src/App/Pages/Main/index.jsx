import React, {Component} from 'react';

import RoundComposition from './RoundComposition/index.jsx';
import Moon from './Moon/index.jsx';
import InformationSelection from './InformationSelection/index.jsx';

class Main extends Component{
    render() {
        return<main>
            <RoundComposition/>
            <Moon/>
            <InformationSelection/>
        </main>
    }
}

export default Main;
