import React, {Component} from 'react';
import './style.scss'
import {zodiacSign} from "../../../DB";

class RoundComposition extends Component {
    render() {

        return <nav className='round-composition-content'>
            <div className='round'>
                {
                    zodiacSign.map((sign, index) => {
                        return <div className={'zodiac'+index} key={index}>
                            <div className='name'>{sign.name}</div>
                            <img alt={sign.name} className='img' src={sign.img}/>
                        </div>
                    })
                }
            </div>
        </nav>
    }
}

export default RoundComposition;
