import React, {Component} from 'react';
import './style.scss'
import {zodiacSign} from './../../../DB'

class EditHoroscope extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {

        return <div className='edit-horoscope-content'>
            <div className='container'>
                <form className='signs'>
                    {
                        zodiacSign.map((text, index) => {
                            return <div className='title' key={index}><input name='sign' type='radio'/>{text.name}</div>
                        })
                    }
                </form>
                <div className='edit-fields'>
                    <div className="block">
                        <div className="title">Text</div>
                        <textarea/>
                    </div>
                    <div className="block">
                        <div className="title">Date</div>
                        <input/>
                    </div>
                    <button className='send'>Send</button>
                </div>

            </div>
        </div>
    }
}

export default EditHoroscope;
