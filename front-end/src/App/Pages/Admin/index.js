import React, {Suspense} from 'react';
import {hot} from "react-hot-loader";
import {Switch, Route, Redirect} from 'react-router-dom';

import MainAdmin from './MainAdmin/index'
import EditHoroscope from './EditHoroscope/index'

class App extends React.Component {

    render() {
        return <Switch>
            <Route path='/admin/EditHoroscope' component={EditHoroscope}/>
            <Route path='/admin' component={MainAdmin}/>
        </Switch>

    }
}

export default hot(module)(App);
