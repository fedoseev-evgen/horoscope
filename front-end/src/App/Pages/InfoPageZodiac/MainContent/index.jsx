import React, {Component} from 'react';
import './style.scss';
import {Link} from "react-router-dom";

import eye from './../../../Static/Media/eys.png'

class MainContent extends Component {
    render() {
        return <div className='main-content-content'>

            <div className='zodiac-header'>
                <div className='zodiac-header-left'>
                    <img/>
                    <div className='big-info'>
                        <h2>Рак</h2>
                        <h3>5 мая</h3>
                    </div>
                </div>
                <div className='number-of-views'>
                    <img src={eye}/>
                    просмотров
                </div>
            </div>
            <pre>
                егодня звезды гороскопа способны не только наделить вас активностью и энергией, но и порадовать
                приятными сюрпризами, в том числе материальными. Вообще, гороскоп сегодня склоняет к тому, чтобы
                заняться финансовой сферой: навести порядок в денежных делах, сделать расчеты на будущее, принять
                решение о крупных покупках, отправиться по магазинам. Не исключено получение прибыли: какие-то из ваших
                проектов способны принести дивиденды. А вот на азартные игры и слепое везение рассчитывать не стоит –
                день способен принести удачу и прибыль
            </pre>
            <div className='other-link'>
                <Link to='/'>
                    <span className='text'>ссылка </span>
                    <span className='date'>5 мая</span>
                </Link>
                <Link to='/'>
                    <span className='text'>ссылка </span>
                    <span className='date'>5 мая</span>
                </Link>
                <Link to='/'>
                    <span className='text'>ссылка </span>
                    <span className='date'>5 мая</span>
                </Link>
                <Link to='/'>
                    <span className='text'>ссылка </span>
                    <span className='date'>5 мая</span>
                </Link>

            </div>

        </div>
    }
}

export default MainContent;
