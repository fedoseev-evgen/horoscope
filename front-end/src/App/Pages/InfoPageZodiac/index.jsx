import React, {Component} from 'react';

import Ads from './Ads/index.jsx'
import MainContent from './MainContent/index.jsx'
import SideBar from './SideBar/index.jsx'
import './style.scss'


class InfoPageZodiac extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sign: 0,
            time: 0
        };

    }

    componentDidMount() {
        scrollTo(0,0);
    }

    render() {
        return <main className='info-page-zodiac-content'>
            <div className='container'>
                <SideBar/>
                <MainContent/>
                <Ads/>
            </div>

        </main>
    }
}

export default InfoPageZodiac;
