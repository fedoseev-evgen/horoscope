import React, {Component} from 'react';
import './style.scss';
import line from './../../../Static/Media/Line.png'
import {zodiacSign} from "../../../DB";

class Ads extends Component{
    render() {

        return<div className='side-bar-content'>
            {
                zodiacSign.map((sign, index) => {
                    return <div className='zodiac' key={index}>
                        <img className='icon' src={sign.img}/>
                        <div className='name'>{sign.name}</div>
                    </div>
                })
            }
            <img className='line' src={line}/>
        </div>
    }
}

export default Ads;
