import Aquarius from './Static/Media/Aquarius_34528.png'
import Aries from './Static/Media/Aries_34527.png'
import Cancer from './Static/Media/Cancer_34526.png'
import Capricorn from './Static/Media/Capricorn_34525.png'
import Gemini from './Static/Media/Gemini_34524.png'
import Leo from './Static/Media/Leo_34523.png'
import Libra from './Static/Media/Libra_34522.png'
import Pisces from './Static/Media/Pisces_34521.png'
import Sagittarius from './Static/Media/Sagittarius_34520.png'
import Scorpio from './Static/Media/Scorpio_34519.png'
import Taurus from './Static/Media/Taurus_34518.png'
import Virgo from './Static/Media/Virgo_34517.png'

export let zodiacSign = [
    {
        name: 'Козерог',
        img: Capricorn
    },
    {
        name: 'Водолей',
        img: Aquarius
    },
    {
        name: 'Рыба',
        img: Pisces
    },
    {
        name: 'Овен',
        img: Aries
    },
    {
        name: 'Телец',
        img: Taurus
    },
    {
        name: 'Близнецы',
        img: Gemini
    },
    {
        name: 'Рак',
        img: Cancer
    },
    {
        name: 'Лев',
        img: Leo
    },
    {
        name: 'Дева',
        img: Virgo
    },
    {
        name: 'Весы',
        img: Libra
    },
    {
        name: 'Скорпион',
        img: Scorpio
    },
    {
        name: 'Стрелец',
        img: Sagittarius
    },
];
