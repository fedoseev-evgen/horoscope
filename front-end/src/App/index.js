import React, {Suspense} from 'react';
import {hot} from "react-hot-loader";
import {Switch, Route, Redirect} from 'react-router-dom';
import './main.scss';

import Header from './Components/Header/Header.jsx';
import Footer from './Components/Footer.jsx';
import Main from './Pages/Main/index.jsx';
import InfoPageZodiac from './Pages/InfoPageZodiac/index.jsx';

import Admin from './Pages/Admin/index'


class App extends React.Component {

    render() {
        return <div className="SiteWrapper">
            <Header/>
            <Switch>
                <Route path='/admin' component={Admin}/>
                <Route path='/:sign/:time' component={InfoPageZodiac}/>
                <Route path='/' component={Main}/>
            </Switch>
            <Footer/>

            <div className='parallax'>
                <div className='stars'/>
                <div className='point-stars'/>
            </div>
        </div>
    }
}

export default hot(module)(App);
