import React, {Component} from 'react';
import './style.scss';
import '../../services';
import {getToday} from "../../services";
import {Link} from "react-router-dom";

class Header extends Component {
    render() {

        return <header className='header-content'>
            <div className='container'>
                <Link to='/'>
                    <h1 className='main-name'><span className='world'>Мир </span>гороскопов</h1>
                </Link>
                <div className='date'><span className='today'>Сегодня </span>{getToday()}</div>
            </div>
        </header>
    }
}

export default Header;
